
public class Konfigurationstest 
{

	public static void main(String[] args) 
	{
		// Variablen
		
			// �bung 1
			// a)
			int cent = 70;
			cent = 80;
			// b)
			double maximum = 95.50;
			
			// �bung 2
			boolean a = true;
			short b = -1000;
			float c = 1.255f;
			char d = '#';
			
			// �bung 3
			// a)
			String test = "Cihan Aziz Kacar";
			// b)
			final short check_nr = 8765;
			
			//�bung 4
			// Datentypen in Programmiersprachen ist sinnvoll, um Speicherplatz zu spraren und eine 
			// Verbesserung der Rechenleistung zu bieten.
		
		// Operatoren
			
			// �bung 1
			// a)
			int ergebnis = 4 + 8 * 9 - 1;
			System.out.println(ergebnis);
			
			// b)
			int zaeler = 1;
			zaeler++;
			System.out.println(zaeler);
			
			// c)
			int ergebnis_2 = 22/6;
			System.out.println(ergebnis_2);
		
			// �bung 2
			// a)
			byte schalter = 10;
			boolean cihan = schalter > 7 && schalter < 12;
			System.out.println(cihan);
			
			// b)
			boolean aziz = schalter != 10 || schalter == 12;
			System.out.println(aziz);
			
			// �bung 3
			String zeichenkette = "Meine Oma " + "f�hrt im " + "H�hnerstall Motorrad.";
			System.out.println(zeichenkette);
	}

}
