﻿import java.util.Scanner;
public class Fahrkartenautomat {

	public static void main(String[] args) {
		
		while(true) {
			System.out.printf("Ihr Restgeld beträgt: %.2f" + "Euro", fahrkartenBezahlen(fahrkartenbestellungErfassen()));
			fahrkartenAusgeben();
		}

	}
	
	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		double ticketPreis = 1.50;
		if(ticketPreis < 0) {
			System.out.println("Fehler: Der Ticketpreis darf nicht negativ sein. Der Wert wurde auf '1,50' zurückgesetzt!");
			ticketPreis = 1.50;
		}
		int anzahl_Ticket;
		double zuZahlenderBetrag = 0;
		
		System.out.println("Ticketpreis: " + ticketPreis + "€");
		System.out.println("Wie viele Tickets benötigen Sie? Geben Sie die Anzahl bitte ein!");
		anzahl_Ticket = tastatur.nextInt();
		if(anzahl_Ticket > 10) {
			System.out.println("Fehler: Sie können nicht mehr als 10 Tickets gleichzeitig kaufen. Der Wert wurde auf '1' gesetzt.");
			anzahl_Ticket = 1;
		}
		
		double arr = zuZahlenderBetrag = ticketPreis * anzahl_Ticket;
		
	
		return arr;
		
	}
	
	public static double fahrkartenBezahlen(double zuZahlen) {
		double rückgabebetrag, eingezahlterGesamtbetrag = 0, zuZahlenderBetrag = zuZahlen, eingeworfeneMünze;
		Scanner tastatur = new Scanner(System.in);
		eingezahlterGesamtbetrag = 0.0;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("Noch zu zahlen: %.2f" + "Euro", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	   System.out.print("\nEingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       }
	       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(rückgabebetrag > 0.0) {
	    	   rückgeldAusgeben(rückgabebetrag);
	    	   
	       }
	       return rückgabebetrag;
		
	}
	public static double rückgeldAusgeben(double rückgabebetrag) {

	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f" + "Euro",rückgabebetrag);
	    	   System.out.println("\nwird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println(muenzeAusgabe(2, "Euro"));
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println(muenzeAusgabe(1, "Euro"));
		          rückgabebetrag -= 0.99;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println(muenzeAusgabe(50, "Cent"));
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println(muenzeAusgabe(20, "Cent"));
	 	          rückgabebetrag -= 0.19;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println(muenzeAusgabe(10, "Cent"));
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println(muenzeAusgabe(5, "Cent"));
	 	          rückgabebetrag -= 0.05;
	           }
	       }
	       return rückgabebetrag;
	       
	}
	public static void fahrkartenAusgeben() {

			System.out.println("\nFahrschein wird ausgegeben");
		       for (int i = 0; i < 8; i++)
		       {
		          System.out.print("=");
		          warte(250);
		       }
		       System.out.println("\n\n");

		}
	public static void warte(int millisekunden) {
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static String muenzeAusgabe(int betrag, String einheit) {
		String s = betrag + " " + einheit;
		return s;
		
		
	}
}

