import java.util.Scanner;

public class HelloWorld 
{

	public static void main(String[] args) 
	{
		wuerfel(0);
		quader(0, 0, 0);
		pyramide(0, 0);
		kugel(0);
		
	}
	
	
	public static void wuerfel(double a) 
	{
		System.out.println("W�rfel:");
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl ein, wovon Sie den Volumen berechnen wollen: ");
		a = tastatur.nextDouble();
		double volumen = a * a * a;
		System.out.println("Das Volumen vom W�rfel betr�gt: " + volumen);
		return;
	}
	
	public static void quader(double a, double b, double c) 
	{
		System.out.println("\n\nQuader: ");
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl ein, wovon Sie den Volumen berechnen wollen: ");
		a = tastatur.nextDouble();
		b = tastatur.nextDouble();
		c = tastatur.nextDouble();
		double volumen = a * b * c;
		System.out.println("Das Volumen vom Quader betr�gt: " + volumen);
		return;
	}
	
	public static void pyramide(double a, double h) 
	{
		System.out.println("\n\nPyramide: ");
		Scanner tastatur = new Scanner(System.in);
		a = tastatur.nextDouble();
		h = tastatur.nextDouble();
		double volumen = a * a * h/3;
		System.out.println("Das Volumen von der Pyramide betr�gt: " + volumen);
		return;
	}
	
	public static void kugel(double r) 
	{
		System.out.println("\n\nKugel: ");
		Scanner tastatur = new Scanner(System.in);
		r = tastatur.nextDouble();
		double pi = 3.14159265359;
		double volumen = (4.0/3.0) * Math.pow(r, 3)  * pi;
		System.out.println("Das Volumen von der Kugel betr�gt: " + volumen);
		return;
	}
	

}
