import java.util.Scanner;

public class BMI {

	public static void main(String[] args) 
	{
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie ihr K�rpergewicht in kg ein: "); 
		int gewicht = myScanner.nextInt(); 
		
		System.out.print("Bitte geben Sie ihre K�rpergr��e in cm ein: ");
		int groesse = myScanner.nextInt(); 
		
		System.out.print("Bitte geben Sie ihr Geschlecht an: ");
		char geschlecht = myScanner.next().charAt(0); 
		
		double bmi = gewicht / ((groesse/100.0)*(groesse/100.0));
		//System.out.printf("%.1f", bmi);
		
		
		if(geschlecht == 'm')
		{
			if(bmi<20)
			{
				System.out.printf("Sie sind Untergewichtig und Ihr BMI betr�gt: " + "%.1f",bmi);
			}
			else if (bmi >20 && bmi <25)
			{
				System.out.printf("Sie haben einen Normalgewicht und ihr BMI betr�gt: " + "%.1f",bmi);
			}
			else if (bmi>25)
			{
				System.out.printf("Sie sind �bergewichtig und Ihr BMI betr�gt: " + "%.1f",bmi);
			}
		}
		else if(geschlecht == 'w')
		{
			if(bmi<19)
			{
				System.out.printf("Sie sind Untergewichtig und Ihr BMI betr�gt: " + "%.1f",bmi);
			}
			else if (bmi>19 && bmi <24)
			{
				System.out.printf("Sie haben einen Normalgewicht und ihr BMI betr�gt: " + "%.1f",bmi);
			}
			else if (bmi>24)
			{
				System.out.printf("Sie sind �bergewichtig und Ihr BMI betr�gt: " + "%.1f",bmi);
			}
		}
		

	}

}
