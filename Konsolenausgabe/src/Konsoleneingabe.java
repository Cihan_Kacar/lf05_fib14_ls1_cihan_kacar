import java.util.Scanner; // Import der Klasse Scanner

public class Konsoleneingabe
{
	public static void main(String[] args)
	{
		 // Neues Scanner-Objekt myScanner wird erstellt 
		 Scanner myScanner = new Scanner(System.in); 
		 
		 // Addition
		 System.out.println("Addition");
		 System.out.print("Bitte geben Sie eine ganze Zahl ein: "); 
		 
		 // Die Variable zahl1 speichert die erste Eingabe
		 int summand1 = myScanner.nextInt(); 
		 
		 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		 
		 // Die Variable zahl2 speichert die zweite Eingabe
		 int summand2 = myScanner.nextInt(); 
		 
		 // Die Addition der Variablen zahl1 und zahl2 
		 // wird der Variable ergebnis zugewiesen.
		 int summe = summand1 + summand2;
		 
		 System.out.print("\n\n\nErgebnis der Addition lautet: ");
		 System.out.print(summand1 + " + " + summand2 + " = " + summe); 
		 
		 
		 // Subtraktion
		 System.out.println("\n\n\nSubtraktion");
		 System.out.print("Bitte geben Sie eine ganze Zahl ein: "); 
		 
		 int minuend = myScanner.nextInt(); 
		 
		 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		 
		 int subtrahend = myScanner.nextInt(); 
		 
		 int differenz = minuend - subtrahend;
		 
		 System.out.print("\n\n\nErgebnis der Subtraktion lautet: ");
		 System.out.print(minuend + " - " + subtrahend + " = " + differenz);
		 
		 
		 // Multiplikation
		 System.out.println("\n\n\nMultiplikation");
		 System.out.print("Bitte geben Sie eine ganze Zahl ein: "); 
				 
		 int faktor1 = myScanner.nextInt(); 
				 
		 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
				 
		 int faktor2 = myScanner.nextInt(); 
			
		 int produkt = faktor1 * faktor2;
				 
		 System.out.print("\n\n\nErgebnis der Multiplikation lautet: ");
		 System.out.print(faktor1 + " * " + faktor2 + " = " + produkt);
		 
		 
		 // Division
		 System.out.println("\n\n\nDivision");
		 System.out.print("Bitte geben Sie eine ganze Zahl ein: "); 
						 
		 int dividend = myScanner.nextInt(); 
						
		 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
						 
		 int divisor = myScanner.nextInt(); 
					
		 int quotient = dividend / divisor;
						 
		 System.out.print("\n\n\nErgebnis der Division lautet: ");
		 System.out.print(dividend + " : " + divisor + " = " + quotient);
		 
		 
		 myScanner.close();
		 
	}

}


 

